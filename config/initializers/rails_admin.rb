# RailsAdmin config file. Generated on September 25, 2012 09:05
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|

  config.authorize_with :cancan, AdminAbility

  # If your default_local is different from :en, uncomment the following 2 lines and set your default locale here:
  require 'i18n'
  I18n.default_locale = :es

  config.current_user_method { current_user } # auto-generated

  # If you want to track changes on your models:
  # config.audit_with :history, User

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, User

  # Set the admin name here (optional second array element will appear in a beautiful RailsAdmin red ©)
  config.main_app_name = ['Linevet', 'Admin']
  # or for a dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }


  #  ==> Global show view settings
  # Display empty fields in show views
  # config.compact_show_view = false

  #  ==> Global list view settings
  # Number of default rows per-page:
  # config.default_items_per_page = 20

  #  ==> Included models
  # Add all excluded models here:
  # config.excluded_models = [AlergiesPet, Clinic, Comment, DeparasitationPet, DiseasePet, Event, MedicationPet, Pet, SterilizationPet, TeethPet, User, VaccinePet]

  # Add models here if you want to go 'whitelist mode':
  # config.included_models = [AlergiesPet, Clinic, Comment, DeparasitationPet, DiseasePet, Event, MedicationPet, Pet, SterilizationPet, TeethPet, User, VaccinePet]

  # Application wide tried label methods for models' instances
  # config.label_methods << :description # Default is [:name, :title]

  #  ==> Global models configuration
  # config.models do
  #   # Configuration here will affect all included models in all scopes, handle with care!
  #
  #   list do
  #     # Configuration here will affect all included models in list sections (same for show, export, edit, update, create)
  #
  #     fields_of_type :date do
  #       # Configuration here will affect all date fields, in the list section, for all included models. See README for a comprehensive type list.
  #     end
  #   end
  # end
  #
  #  ==> Model specific configuration
  # Keep in mind that *all* configuration blocks are optional.
  # RailsAdmin will try his best to provide the best defaults for each section, for each field.
  # Try to override as few things as possible, in the most generic way. Try to avoid setting labels for models and attributes, use ActiveRecord I18n API instead.
  # Less code is better code!
  # config.model MyModel do
  #   # Cross-section field configuration
  #   object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #   label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #   label_plural 'My models'      # Same, plural
  #   weight -1                     # Navigation priority. Bigger is higher.
  #   parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #   navigation_label              # Sets dropdown entry's name in navigation. Only for parents!
  #   # Section specific configuration:
  #   list do
  #     filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #     items_per_page 100    # Override default_items_per_page
  #     sort_by :id           # Sort column (default is primary key)
  #     sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     # Here goes the fields configuration for the list view
  #   end
  # end

  # Your model's configuration, to help you get started:

  # All fields marked as 'hidden' won't be shown anywhere in the rails_admin unless you mark them as visible. (visible(true))

  # config.model AlergiesPet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association 
  #     configure :clinic, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :clinic_id, :integer         # Hidden 
  #     configure :name, :string 
  #     configure :reaction, :text 
  #     configure :treatment, :text 
  #     configure :notes, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Clinic do
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
  #     configure :name, :string 
  #     configure :location, :string 
  #     configure :phone, :integer 
  #     configure :email, :string 
  #     configure :person_in_charge, :string 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 
  #     configure :avatar_file_name, :string 
  #     configure :avatar_content_type, :string 
  #     configure :avatar_file_size, :integer 
  #     configure :avatar_updated_at, :datetime 
  #     configure :photo_file_name, :string 
  #     configure :photo_content_type, :string 
  #     configure :photo_file_size, :integer 
  #     configure :photo_updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Comment do
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
  #     configure :event_id, :integer 
  #     configure :owner_id, :integer 
  #     configure :text, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model DeparasitationPet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :dep_type, :boolean 
  #     configure :dep_brand, :string 
  #     configure :dep_date, :date 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model DiseasePet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association 
  #     configure :clinic, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :clinic_id, :integer         # Hidden 
  #     configure :name, :integer 
  #     configure :cronic, :boolean 
  #     configure :diagnostic_date, :date 
  #     configure :treatment, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Event do
  #   # Found associations:
  #     configure :pet, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :event_type, :string 
  #     configure :date, :date 
  #     configure :event_text, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 
  #     configure :photo_file_name, :string         # Hidden 
  #     configure :photo_content_type, :string         # Hidden 
  #     configure :photo_file_size, :integer         # Hidden 
  #     configure :photo_updated_at, :datetime         # Hidden 
  #     configure :photo, :paperclip   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model MedicationPet do
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer 
  #     configure :clinic_id, :integer 
  #     configure :name, :string 
  #     configure :purpose, :text 
  #     configure :dose, :text 
  #     configure :freq, :integer 
  #     configure :freq_per, :string 
  #     configure :start_date, :date 
  #     configure :end_date, :date 
  #     configure :note, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model Pet do
  #   # Found associations:
  #     configure :user, :belongs_to_association 
  #     configure :vaccine_pet, :has_many_association 
  #     configure :alergies_pet, :has_many_association 
  #     configure :event, :has_many_association 
  #     configure :deparasitation_pet, :has_many_association 
  #     configure :disease_pet, :has_many_association 
  #     configure :teeth_pet, :has_many_association 
  #     configure :medication_pet, :has_many_association 
  #     configure :sterilization_pet, :has_many_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :user_id, :integer         # Hidden 
  #     configure :name, :string 
  #     configure :gender, :boolean 
  #     configure :specie, :string 
  #     configure :race, :string 
  #     configure :b_date, :date 
  #     configure :weight_last, :float 
  #     configure :weight_actual, :float 
  #     configure :food_type, :string 
  #     configure :food_dosis, :integer 
  #     configure :food_extras, :string 
  #     configure :exercise_type, :string 
  #     configure :exercise_freq, :integer 
  #     configure :exercise_freq_per, :string 
  #     configure :exercise_location, :string 
  #     configure :id_rabies, :integer 
  #     configure :id_pedigree, :integer 
  #     configure :id_microchip, :integer 
  #     configure :hair_dreeser, :string 
  #     configure :hair_type, :string 
  #     configure :string, :string 
  #     configure :hair_freq_frec, :integer 
  #     configure :hair_brush_freq_per, :string 
  #     configure :hair_cut_type, :string 
  #     configure :bath_hairdresser, :string 
  #     configure :bath_last, :date 
  #     configure :bath_next, :date 
  #     configure :san_bath, :string 
  #     configure :san_bath_last, :date 
  #     configure :san_bath_next, :date 
  #     configure :san_bath_diag, :string 
  #     configure :san_bath_prod, :string 
  #     configure :san_bath_freq, :integer 
  #     configure :san_bath_freq_per, :string 
  #     configure :nail_cut_last, :date 
  #     configure :nail_cut_next, :date 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 
  #     configure :avatar_file_name, :string         # Hidden 
  #     configure :avatar_content_type, :string         # Hidden 
  #     configure :avatar_file_size, :integer         # Hidden 
  #     configure :avatar_updated_at, :datetime         # Hidden 
  #     configure :avatar, :paperclip   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model SterilizationPet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association 
  #     configure :clinic, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :clinic_id, :integer         # Hidden 
  #     configure :sterilyzed, :boolean 
  #     configure :sterilyzed_date, :date 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model TeethPet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association 
  #     configure :clinic, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :clinic_id, :integer         # Hidden 
  #     configure :procedure, :string 
  #     configure :last_procedure, :date 
  #     configure :next_procedure, :date 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model User do
  #   # Found associations:
  #     configure :pets, :has_many_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :email, :string 
  #     configure :password, :password         # Hidden 
  #     configure :password_confirmation, :password         # Hidden 
  #     configure :reset_password_token, :string         # Hidden 
  #     configure :reset_password_sent_at, :datetime 
  #     configure :remember_created_at, :datetime 
  #     configure :sign_in_count, :integer 
  #     configure :current_sign_in_at, :datetime 
  #     configure :last_sign_in_at, :datetime 
  #     configure :current_sign_in_ip, :string 
  #     configure :last_sign_in_ip, :string 
  #     configure :confirmation_token, :string 
  #     configure :confirmed_at, :datetime 
  #     configure :confirmation_sent_at, :datetime 
  #     configure :unconfirmed_email, :string 
  #     configure :failed_attempts, :integer 
  #     configure :unlock_token, :string 
  #     configure :locked_at, :datetime 
  #     configure :authentication_token, :string 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 
  #     configure :role, :string 
  #     configure :location, :string 
  #     configure :name, :string 
  #     configure :address, :string 
  #     configure :phone, :string 
  #     configure :gender, :boolean 
  #     configure :avatar_file_name, :string 
  #     configure :avatar_content_type, :string 
  #     configure :avatar_file_size, :integer 
  #     configure :avatar_updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
  # config.model VaccinePet do
  #   # Found associations:
  #     configure :pet, :belongs_to_association 
  #     configure :clinic, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :pet_id, :integer         # Hidden 
  #     configure :clinic_id, :integer         # Hidden 
  #     configure :name, :string 
  #     configure :vaccine_id, :integer 
  #     configure :vaccine_date, :date 
  #     configure :next_date, :date 
  #     configure :notes, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  # end
end
