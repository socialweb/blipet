# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ()->
  $("form.new_event").on "ajax:success", (event, data, status, xhr) ->
    $('#new-event-modal').modal('hide')
    window.location.reload();