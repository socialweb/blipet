# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$('#search').tooltip()

$('#pet_link').click(=> 
  $('#clinics_search').hide('fast')
  $('#pets_search').show('fast')
  $('#pet_ul').addClass('active')
  $('#cli_ul').removeClass('active')
)

$('#cli_link').click( =>
  $('#pets_search').hide('fast')
  $('#clinics_search').show('fast') 
  $('#cli_ul').addClass('active')
  $('#pet_ul').removeClass('active')
)
