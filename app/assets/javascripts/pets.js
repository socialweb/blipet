$(function(){


function Arrow_Points()
{ 
var s = $('#container').find('.item');
$.each(s,function(i,obj){
var posLeft = $(obj).css("left");
$(obj).addClass('borderclass');
if(posLeft == "0px")
{
html = "<span class='linea izq'></span><span class='iconoTimeline rightCorner'></span>";
$(obj).prepend(html);
$(obj).addClass('izquierda');
}
else
{
html = "<span class='iconoTimeline leftCorner'></span><span class='linea der'></span>";
$(obj).prepend(html);
$(obj).addClass('derecha');
}
$("#container").css({visibility: "visible"});
});
}

$('.timeline_container').mousemove(function(e)
{
var topdiv=$("#containertop").height();
var pag= e.pageY - topdiv-26;
$('.plus').css({"top":pag +"px", "background":"url('/assets/plus.png')","margin-left":"1px"});}).
mouseout(function()
{
$('.plus').css({"background":"url('')"});
});
  
  
        
$("#update_button").live('click',function()
{
var x=$("#update").val();
$("#container").prepend('<div class="item"><a href="#" class="deletebox">X</a><div>'+x+'</div></div>');

//Reload masonry
$('#container').masonry( 'reload' );

$('.rightCorner').hide();
$('.leftCorner').hide();
Arrow_Points();

$("#update").val('');
$("#popup").hide();
return false;
});


//Confirmar Nuevo Evento
$("#confirmar_nuevoEvento").live('click',function()
{
var x=$("#texto_nuevoEvento").val();


$("#container").prepend('<div class="item"><a href="#" class="deletebox">X</a><div>'+x+'</div></div>');

//Reload masonry
$('#container').masonry( 'reload' );

$('.rightCorner').hide();
$('.leftCorner').hide();
Arrow_Points();

$("#texto_nuevoEvento").val('');
//$("#popup").hide();
$('#modal_nuevoEvento').modal('hide');

return false;
});




// Divs
$('#container').masonry({itemSelector : '.item',});
Arrow_Points();
  
//Mouseup textarea false
$("#popup").mouseup(function() 
{
return false
});
  
$(".timeline_container").click(function(e)
{
var topdiv=$("#containertop").height();
$("#popup").css({'top':(e.pageY-topdiv-33)+'px'});
$("#popup").fadeIn();
$("#update").focus();

  
});

//Agregar Evento
$(".agregar_evento").click(function(e)
{
  $('#new-event-modal').modal('show');
});


$(".deletebox").live('click',function()
{
if(confirm("Are your sure?"))
{
$(this).parent().fadeOut('slow');  
//Remove item
$('#container').masonry( 'remove', $(this).parent() );
//Reload masonry
$('#container').masonry( 'reload' );
$('.rightCorner').hide();
$('.leftCorner').hide();
Arrow_Points();
}
return false;
});



//Textarea without editing.
$(document).mouseup(function()
{
$('#popup').hide();

});
 
$(".form-noenter").bind("keypress", function(e) {
	if (e.keyCode == 13) return false;
});






var dog = ["Affenpinscher","Airedale Terrier","Akita","Akita americano","Alaskan Husky","Alaskan Malamute","American Foxhound","American pit bull terrier","American staffordshire terrier","Antiguo perro de muestra danés","Ariegeois","Artois","Australian silky terrier","Australian terrier","Austrian Black & Tan Hound","Azawakh","Balkan Hound","Basenji","Basset Alpino (Alpine Dachsbracke)","Basset Artesiano Normando","Basset Azul de Gascuña (Basset Bleu de Gascogne)","Basset de Artois","Basset de Westphalie","Basset Hound","Basset Leonado de Bretaña (Basset fauve de Bretagne)","Bavarian Mountain Scenthound","Beagle","Beagle Harrier","Beauceron","Bedlington Terrier","Bichón boloñés","Bichón frisé","Bichón habanero","Bichón maltés","Billy","Black and Tan Coonhound","Bloodhound (Sabueso de San Huberto)","Bobtail","Boerboel","Border Collie","Border Terrier","Borzoi","Bosnian Hound","Boston terrier","Bouvier des Flandres","Boxer","Boyero de Appenzell","Boyero de Australia","Boyero de Entlebuch","Boyero de las Ardenas","Boyero de Montaña Bernes","Braco alemán de pelo corto","Braco alemán de pelo duro","Braco de Ariege","Braco de Auvernia","Braco de Bourbonnais","Braco Saint-Germain","Braco de Weimar","Braco francés tipo Gascuña","Braco francés tipo Pirineos","Braco húngaro de pelo corto","Braco húngaro de pelo duro","Braco italiano","Broholmer","Buhund Noruego","Bull terrier","Bulldog","Bulldog americano","Bulldog francés","Bullmastiff","Ca de Bestiar","Cairn terrier","Cane Corso","Cane da Pastore Maremmano-Abruzzese","Caniche","Cao da Serra de Aires","Cao da Serra de Estrela","Cao de Castro Laboreiro","Cao de Fila de Sao Miguel","Cavalier King Charles Spaniel","Cesky Fousek","Ceský teriér","Chesapeake Bay Retriever","Chihuahua","Chin","Chow Chow","Cirneco del Etna","Clumber Spaniel","Cocker Spaniel Americano","Cocker Spaniel Inglés","Collie Barbudo","Collie de Pelo Corto","Collie de Pelo Largo","Coton de Tuléar","Curly Coated Retriever","Dálmata","Dandie dinmont terrier","Deerhound","Dobermann","Dogo Argentino","Dogo de Burdeos","Dogo del Tibet","Drentse Partridge Dog","Drever","Dunker","Elkhound Noruego","Elkhound Sueco","English Foxhound","English Springer Spaniel","English toy terrier","Epagneul Picard","Eurasier","Fila Brasileiro","Finnish Lapphound","Flat Coated Retriever","Fox terrier de pelo de alambre","Fox terrier de pelo liso","Foxhound Inglés","Galgo Español","Galgo húngaro (Magyar Agar)","Galgo Italiano","Galgo Polaco (Chart Polski)","Glen of Imaal Terrier","Golden Retriever","Gordon Setter", "Gos d'Atura Catalá", "Gran Basset Griffon Vendeano","Gran Boyero Suizo","Gran Danés (Dogo Aleman)","Gran Gascón Saintongeois","Gran Griffon Vendeano","Gran Munsterlander","Gran Perro Japonés","Grand Anglo Francais Tricoleur","Grand Bleu de Gascogne","Greyhound","Griffon Bleu de Gascogne","Griffon de pelo duro (Grifón Korthals)","Griffon leonado de Bretaña","Griffon Nivernais","Grifón belga","Grifón de Bruselas","Haldenstoever","Harrier","Hokkaido","Hovawart","Ioujnorousskaia Ovtcharka","Irish Glen of Imaal terrier","Irish soft coated wheaten terrier","Irish terrier","Irish Water Spaniel","Irish Wolfhound","Kai","Keeshond","Kelpie Australiano (Australian Kelpie)","Kerry blue terrier","King Charles Spaniel","Kishu","Komondor","Kooiker","Kromfohrländer","Kuvasz","Labrador Retriever","Lagotto Romagnolo","Laika de Siberia Occidental","Laika de Siberia Oriental","Laika Ruso Europeo","Lakeland terrier","Landseer","Lapphund Sueco","Lebrel Afgano","Lebrel Arabe (Sloughi)","Leonberger","Lhasa apso","Lowchen","Lundehund Noruego","Malamute de Alaska","Manchester terrier","Mastiff","Mastín de los Pirineos","Mastín Español","Mastín Napolitano","Mudi","Norfolk terrier","Norwich terrier","Nova Scotia duck tolling retriever","Ovejero alemán","Otterhound","Parson Russell terrier","Pastor Alemán","Pastor Australiano","Pastor Belga","Pastor Belga Groenendael","Pastor Belga Laekenois","Pastor Belga Malinois","Pastor Belga Tervueren","Pastor Bergamasco","Pastor blanco suizo (Berger Blanc Suisse)","Pastor Croata","Pastor de Anatolia","Pastor de Asia Central","Pastor de Brie","Pastor de Istria","Pastor de Laponia (Laponian Herder)","Pastor de los Pirineos de pelo largo","Pastor de los Pirineos de cara rasa","Pastor de Shetland","Pastor del Atlas (Aidi)","Pastor del Cáucaso","Pastor catalán","Pastor Holandés","Pastor Islandés","Pastor mallorquín","Pastor Picard","Pastor polaco de las llanuras","Pastor polaco de Podhale","Pastor Vasco","Pekinés","Pequeño brabanzón","Pequeño Munsterlander","Pequeño perro león","Perdiguero de Burgos","Perdiguero Portugués","Perro crestado chino","Perro de Agua Americano (American Water Spaniel)","Perro de Agua Español","Perro de Agua Francés (Barbet)","Perro de Agua Frisón","Perro de Agua Portugués","Perro de Canaan","Perro de Groenlandia","Perro de los Visigodos (Vastgotaspets)","Perro de Montaña de los Pirineos","Perro de Osos de Carelia (Karelian Bear Dog)","Perro de Presa Español","Perro de Presa Mallorquín (Ca de Bou)","Perro del Faraón","Perro Esquimal Americano (American Esquimo Dog)","Perro Esquimal Canadiense","Perro Lobo Checoslovaco","Perro sin pelo de Perú","Petit Basset Griffon Vendeen","Petit Blue de Gascogne ","Pinscher","Pinscher austríaco de pelo corto","Pinscher Miniatura","Pit bull terrier americano","Podenco Andaluz","Podenco Canario","Podenco Ibicenco","Podenco Portugués","Pointer inglés","Poitevin","Pomerania","Porcelana","Presa Canario","Pug","Puli","Pumi","Rafeiro do Alentejo","Ratonero Bodeguero Andaluz","Retriever de Nueva Escocia","Rhodesian Ridgeback","Ridgeback de Tailandia","Rottweiler","Saarloos","Sabueso de Hamilton","Sabueso de Hannover","Sabueso de Hygen","Sabueso de Istria","Sabueso de Posavaz","Sabueso de Schiller (Schillerstovare)","Sabueso de Smaland (Smalandsstovare)","Sabueso de Transilvania","Sabueso del Tirol","Sabueso Español","Sabueso Estirio de pelo duro","Sabueso Finlandés","Sabueso Francés blanco y negro","Sabueso Francés tricolor","Sabueso Griego","Sabueso Polaco (Ogar Polski)","Sabueso Serbio","Sabueso Suizo","Sabueso Yugoslavo de Montaña","Sabueso Yugoslavo tricolor","Saluki","Samoyedo","San Bernardo","Sarplaninac","Schapendoes","Schipperke","Schnauzer","Schnauzer gigante (Riesenschnauzer)","Schnauzer miniatura (Zwergschnauzer)","Scottish terrier","Sealyham terrier","Segugio Italiano","Seppala Siberiano","Setter Inglés","Setter irlandés rojo","Setter irlandés rojo y blanco","Shar Pei","Shiba Inu","Shih Tzu","Shikoku","Skye terrier","Slovensky Cuvac","Slovensky Kopov","Smoushond Holandés","Spaniel Alemán (German Wachtelhund)","Spaniel Azul de Picardía","Spaniel Bretón","Spaniel continental enano","Spaniel de Campo","Spaniel de Pont Audemer","Spaniel Francés","Spaniel japonés","Spaniel tibetano","Spinone Italiano","Spítz Alemán","Spitz de Norbotten (Norbottenspets)","Spitz Finlandés","Spitz Japonés","Staffordshire bull terrier","Staffordshire terrier americano","Sussex Spaniel","Teckel (Dachshund)","Tchuvatch eslovaco","Terranova (Newfoundland)","Terrier australiano (Australian terrier)","Terrier brasilero","Terrier cazador alemán","Terrier checo (Ceský teriér)","Terrier galés","Terrier irlandés (Irish terrier)","Terrier japonés (Nihon teria) ","Terrier negro ruso","Terrier tibetano","Tosa","Viejo Pastor Inglés","Viejo Pointer Danés (Old Danish Pointer)","Vizsla","Volpino Italiano","Weimaraner","Welsh springer spaniel","Welsh Corgi Cardigan","Welsh Corgi Pembroke","Welsh terrier","West highland white terrier","Whippet","Wirehaired solvakian pointer","Xoloitzcuintle","Yorkshire terrier" ];
var cat = ["Azul ruso","American shorthair","American wirehair","Angora turco","Africano","Bengala","Bobtail japonés","Bombay","Bosque de Noruega","British Shorthair","Burmés","Cornish rex","California Spangled","Devon rex","German Rex","Gato","Gato común europeo","Gato exótico","Habana brown","Himalayo","Korat","Maine Coon","Manx","Mau egipcio","Munchkin","Ocicat","Oriental","Oriental de pelo largo","Persa","Peterbald","Ragdoll","Ragamuffin","Sagrado de Birmania","Scottish Fold","Selkirk rex","Serengeti","Seychellois","Siamés","Siamés Moderno","Siamés Tradicional","Siberiano","Snowshoe","Sphynx","Tonkinés","Van Turco"];

var autocomplete = $("#auto_complete").typeahead({source: dog});
$("#pet_clinic_id").typeahead();

$("#pet_specie").change(function(){
	selected = $(this).find(":selected").text();
 	if (selected = "Gato")
		autocomplete.data('typeahead').source = cat;
	else
		autocomplete.data('typeahead').source = dog;
});


});