class CalendarEventsController < ApplicationController
  # GET /calendar_events
  # GET /calendar_events.json
  def index
    @calendar_events = CalendarEvent.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @calendar_events }
    end
  end

  # GET /calendar_events/1
  # GET /calendar_events/1.json
  def show
    @CalendarEvent = CalendarEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @CalendarEvent }
    end
  end

  # GET /calendar_events/1/edit
  def edit
    @CalendarEvent = CalendarEvent.find(params[:id])
  end

  # POST /calendar_events
  # POST /calendar_events.json
  def create
   @CalendarEvent = CalendarEvent.new(params[:calendar_event])

    respond_to do |format|
      if @CalendarEvent.save
        format.html { redirect_to calendar_path, notice: 'Evento calendarizado exitosamente' }
        format.json { render json: @CalendarEvent, status: :created, location: @CalendarEvent }
      else
        format.html { redirect_to calendar_path, alert: 'Evento Aproblemado' }
        format.json { render json: @CalendarEvent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /calendar_events/1
  # PUT /calendar_events/1.json
  def update
    @CalendarEvent = CalendarEvent.find(params[:id])

    respond_to do |format|
      if @CalendarEvent.update_attributes(params[:calendar_event])
        format.html { redirect_to @CalendarEvent, notice: 'CalendarEvent was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @CalendarEvent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calendar_events/1
  # DELETE /calendar_events/1.json
  def destroy
    @CalendarEvent = CalendarEvent.find(params[:id])
    @CalendarEvent.destroy

    respond_to do |format|
      format.html { redirect_to calendar_events_url }
      format.json { head :no_content }
    end
  end



end
