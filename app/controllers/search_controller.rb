class SearchController < ApplicationController
  def index
  	@clinic_results = Clinic.search(params[:search]).paginate(:page => params[:cli_page],:per_page=>10)
  	@pet_results = Pet.search(params[:search]).paginate(:page => params[:pet_page],:per_page=>10)
  end

  def maps
	@json = Clinic.all.to_gmaps4rails do |clinic, marker|
		marker.infowindow render_to_string(:partial => "/search/clinicinfo", :locals => { :object => clinic})
		marker.json({ :id => clinic.id})
	end
  end
end
