class HomeController < ApplicationController
  def index
  	if (user_signed_in? and current_user.role =="owner")
  		redirect_to :controller=>:pets, :action=>:index
  	end
  	if (user_signed_in? and current_user.role =="superadmin")
  		redirect_to "/admin"
  	end
  end
end
