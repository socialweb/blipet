class PetsController < ApplicationController
  # GET /pets
  # GET /pets.json
  before_filter :authenticate_user!, :except => [:show]
  skip_before_filter :require_no_authentication  
  #load_and_authorize_resource
  #skip_authorize_resource :only => [:index,:new,:create,:show]



  def index
    @pets = current_user.pets
    @event = Event.new
    @events = []
    @pets.each do |pet|
      pet.event.each do |ev|
        @events << ev
      end 
    end
    @clinics = []
    for r in Clinic.all do
      @clinics << r.name
    end
    @events = @events.sort_by(&:created_at).reverse
    @pet = Pet.new
    session[:return_to] ||= request.referer
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pets }
    end
  end

  # GET /pets/1
  # GET /pets/1.json
  def show
    @event = Event.new
    @pet = Pet.find(params[:id])
    @events = @pet.event
    session[:return_to] ||= request.referer
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pet }
    end
  end

  # GET /pets/new
  # GET /pets/new.json
  def new
    @pet = Pet.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pet }
    end
  end

  # GET /pets/1/edit
  def edit
    @pet = Pet.find(params[:id])
  end

  # POST /pets
  # POST /pets.json
  def create
    @pet = Pet.new(params[:pet])
    @pet.user_id = current_user.id
    @clinic = Clinic.search(params[:clinic_name]) if params[:clinic_name]
    unless @clinic.nil?
      @pet.clinic_id = @clinic.first.id
    end
    respond_to do |format|
      if @pet.save
        format.html { redirect_to pets_path, notice: 'Mascota agregada exitosamente.' }
        format.json { render json: @pet, status: :created, location: @pet }
      else

        format.html { render action: "index" }
        format.json { render json: @pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pets/1
  # PUT /pets/1.json
  def update
    @pet = Pet.find(params[:id])

    respond_to do |format|
      if @pet.update_attributes(params[:pet])
        format.html { redirect_to @pet, notice: 'Datos actualizados exitosamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pets/1
  # DELETE /pets/1.json
  def destroy
    @pet = Pet.find(params[:id])
    @pet.destroy

    respond_to do |format|
      format.html { redirect_to pets_url }
      format.json { head :no_content }
    end
  end
end
