class ApplicationController < ActionController::Base
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.pets_path, :alert => "No estas autorizado para acceder a este lugar"
  end
end
