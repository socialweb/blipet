class CalendarController < ApplicationController
    before_filter :authenticate_user!
  def index
    @month = (params[:month] || (Time.zone || Time).now.month).to_i
    @year = (params[:year] || (Time.zone || Time).now.year).to_i

    @shown_month = Date.civil(@year, @month)
   
    @pets = current_user.pets

    @event_strips = CalendarEvent.event_strips_for_month(@shown_month, :include => :pet, :conditions =>{"pets.user_id" => current_user.id})

    @calendarevent = CalendarEvent.new

	@selectedce = CalendarEvent.find(params[:selectedid]) unless params[:selectedid].nil?

  end

  def complete
    @calendarevent = CalendarEvent.find(params[:idc])
    unless @calendarevent.nil?
      @calendarevent.refresh_event(params[:repeat])
    end
    redirect_to calendar_path, :notice=>"Evento actualizado exitosamente"
  end

end
