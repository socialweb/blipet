module PetsHelper
  def setup_pet(pet)
    pet.tap do |p|
      p.deparasitation_pet.build if p.deparasitation_pet.empty?
      p.disease_pet.build if p.disease_pet.empty?
      p.vaccine_pet.build if p.vaccine_pet.empty?
    end
  end
end
