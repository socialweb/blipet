module CalendarHelper
  def month_link(month_date)
    link_to(I18n.localize(month_date, :format => "%B"), {:month => month_date.month, :year => month_date.year})
  end
  
  # custom options for this calendar
  def event_calendar_opts
    { 
      :year => @year,
      :month => @month,
      :event_strips => @event_strips,
      :month_name_text => I18n.localize(@shown_month, :format => "%B %Y"),
      :previous_month_text => "<< " + month_link(@shown_month.prev_month),
      :next_month_text => month_link(@shown_month.next_month) + " >>",
      :use_all_day => true
    }
  end

  def event_calendar
    # args is an argument hash containing :event, :day, and :options
    #estilo = %(style="margin-left:-10px;padding-left:10px;background-color:#72C2ED;")
    calendar event_calendar_opts do |args|
      event, day = args[:event], args[:day]
      estilo = ""
      hora = add_cero(event.start_at.hour)+"h "
      if (event.start_at.day < event.end_at.day && event.start_at.month == event.end_at.month) || (event.start_at.day > event.end_at.day && event.start_at.month < event.end_at.month)
        estilo = %(style="margin-left:-11px;padding-left:3px;padding-right:9px;margin-top:-2px;padding-bottom:2px;background-color:#72C2ED;border-radius:5px;")
      elsif event.all_day == true
        estilo = %(style="margin-left:-11px;padding-left:3px;padding-right:9px;margin-top:-2px;padding-bottom:2px;background-color:#37DE7D;border-radius:5px;")
        hora=""
      else
        estilo = %(style="margin-left:-11px;padding-left:3px;padding-right:9px;margin-top:-2px;padding-bottom:2px;")
      end
      html = %(<a class="mod" href="/calendar?selectedid=#{event.id}" #{estilo}>)
      #html << display_event_time(event, day)
      #html << %(#{h(event.name)}</a>)
      html << %(#{hora}#{traducir_evento(event.name)}</a>)
      html
    end
  end

  def ajax_update
    @selectedcalendareventout = CalendarEvent.find(params[:id])
    render :layout => false
  end

  def traducir_evento(event)
    case event
      when "vet"
        return "Veterinario"
      when "vaccine"
        return "Vacuna"
      when "dentist"
        return "Dentista"
      when "exercise"
        return "Ejercicio"
      when "nailcut"
          return "Corte de u&ntilde;as"
      when "deparasitation"
        return "Desparacitar"
      when "hairbrush"
        return "Cepillado"
      when "medication"
        return "Medicamento"
      when "bath"
          return "Ba&ntilde;o"
      when "sanbath"
          return "Ba&ntilde;o sanitario"
      when "foodbuy"
        return "Comida"
      else
        return event
    end
  end

  def add_cero(tiempo)
        if "#{tiempo}".length == 1
          return "0#{tiempo}"
        else
          return "#{tiempo}"
        end
  end

end
