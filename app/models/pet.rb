# encoding: utf-8
class Pet < ActiveRecord::Base
  belongs_to :user
  belongs_to :clinic
  has_many :vaccine_pet, :dependent => :delete_all
  accepts_nested_attributes_for :vaccine_pet, :allow_destroy => true,:reject_if => lambda { |c| c[:name].blank? || c[:vaccine_id].blank? }
  has_many :event, :dependent => :delete_all
  has_many :deparasitation_pet, :dependent => :delete_all
	accepts_nested_attributes_for :deparasitation_pet, :allow_destroy => true,:reject_if => lambda { |c| c[:name].blank?}
  has_many :disease_pet, :dependent => :delete_all
	accepts_nested_attributes_for :disease_pet, :allow_destroy => true,:reject_if => lambda { |c| c[:name].blank?}

  has_many :calendar_event, :dependent => :delete_all

  validates_presence_of :name, :specie, :race, :b_date

  attr_accessible :avatar, :b_date, :bath_hairdresser, 
  :exercise_location, :exercise_type, :clinic_id,
  :food_dosis, :food_extras, :food_type, :food_brand, :gender, 
  :hair_cut_type, :hair_dreeser, :hair_type, :id_microchip, 
  :id_pedigree, :id_rabies, :name, :race, :san_bath, 
  :san_bath_prod, :specie, :weight_actual, :weight_last,:deparasitation_pet_attributes,
  :disease_pet_attributes, :vaccine_pet_attributes, :sterilyzed,:privacy_level
  has_attached_file :avatar,
      :default_url => '/assets/pet_default_:style.png',
      :styles => {
        :thumb => "100x100#",
        :small => "150x150",
        :navbar => "50x50"
      }


  def self.search(search)
    if search
      where('name LIKE ? or race LIKE ?', "%#{search}%","%#{search}%")
    else
      scoped
    end
  end

end
