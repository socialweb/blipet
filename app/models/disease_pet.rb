class DiseasePet < ActiveRecord::Base
  belongs_to :pet
  attr_accessible :cronic, :diagnostic_date, :name, :pet_id, :medication
end