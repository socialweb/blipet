class Sale < ActiveRecord::Base
  attr_accessible :promo_image, :brand, :end_at, :promo_text, :promo_lvl

  has_attached_file :promo_image,
      :styles => {
      	:hero => "450x300#",
        :thumb => "300x200#"
      } 

end
