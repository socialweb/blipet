# encoding: utf-8
class Clinic < ActiveRecord::Base
  attr_accessible :email, :location, :name, :person_in_charge, :phone, :address,:webpage , :latitude, :longitude, :gmaps

  acts_as_gmappable

  def gmaps4rails_address
    [address,location,'Santiago','Chile'].compact.join(', ') unless location.empty?
  end

  def self.search(search)
    if search
      where('name LIKE ? or location LIKE ?', "%#{search}%","%#{search}%")
    else
      scoped
    end
  end

end
