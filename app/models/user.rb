# encoding: utf-8
class User < ActiveRecord::Base

  has_many :pets, :dependent => :delete_all

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :timeoutable, :lockable, :token_authenticatable,
         :confirmable


  validates_presence_of :name, :location
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :role, :location, :name, :address, :phone, :gender, :latitude, :longitude, :avatar, :privacy_level
  # attr_accessible :title, :body

  has_attached_file :avatar,
          :default_url => '/assets/user_default_:style.png',
          :styles => {
            :thumb => "50x50",
            :small => "40x40"
          } 
    
  geocoded_by :location
  after_validation :geocode, :if => :location_changed?

  def role_enum
    ['owner', 'vet', 'superadmin']
  end

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.username = auth.info.nickname
      user.email = auth.info.email
      if auth.info.location
        user.location = auth.info.location
      else
        user.location = "Santiago, Chile"
      end
      user.name = auth.info.name
      user.gender = auth.info.gender
    end
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def self.calendar_status(event, pets)
    unless pets.nil?
      first_item = true
      calendarevent = nil
      if pets.kind_of?(Array)
        pets.each do |pet|
          #nearest calendarevent of the requested type
          temp = pet.calendar_event.where(:complete => false).where(:name => event).sort_by(&:start_at).first      
          unless temp.nil?
            if (first_item)
              calendarevent = temp
              first_item = false
            else
              if calendarevent.start_at > temp.start_at
                calendarevent = temp
              end 
            end
          end
        end
      else
        temp = pets.calendar_event.where(:complete => false).where(:name => event).sort_by(&:start_at).first      
        unless temp.nil?
          if (first_item)
            calendarevent = temp
            first_item = false
          else
            if calendarevent.start_at > temp.start_at
              calendarevent = temp
            end 
          end
        end
      end
      unless (calendarevent.nil?)
        if calendarevent.start_at < DateTime.now
          return "atrasado"
        else
          if calendarevent.start_at > DateTime.now + 3.day
            return "ok"
          else
           return "proximo"
          end
        end
      else
        return "none"
      end
    end
  end


end
