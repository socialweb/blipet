class VaccinePet < ActiveRecord::Base
  belongs_to :pet
  attr_accessible :name, :pet_id, :vaccine_date, :vaccine_id
end
