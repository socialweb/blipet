class Comment < ActiveRecord::Base
  attr_accessible :event_id, :owner_id, :text
end
