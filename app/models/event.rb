class Event < ActiveRecord::Base
belongs_to :pet
attr_accessible :date, :pet_id, :event_text, :event_type, :photo, :latitude, :longitude, :location
geocoded_by :locationb

def locationb
	[location,'Santiago','Chile'].compact.join(', ') unless location.empty?
end

after_validation :geocode, :if => :location_changed?
has_attached_file :photo,
	:styles => {
  	:thumb => "100x100#",
  	:small => "150x150"
	}

def event_type_enum
	['Paseo', 'Veterinaria', 'Deporte', 'Peluqueria', 'Otros']
end

end
