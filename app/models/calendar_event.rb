# encoding: utf-8
class CalendarEvent < ActiveRecord::Base
  has_event_calendar
  belongs_to :pet

  attr_accessible :pet_id, :name, :start_at, :end_at, :all_day, :last_ocurrence, :number_of_times, :every, :complete

  def self.end_at
  	self.start_at + 1.hour
  end

  def self.user_id
    self.pet.user_id
  end

  def refresh_event(repeat)
    if self.number_of_times>0 and repeat === "true"
      newocurrence = self.dup
      newocurrence.save
      self.complete = true
      self.save
      newocurrence = CalendarEvent.last
    	newocurrence.last_ocurrence = newocurrence.start_at
    	if newocurrence.every === "Days"
    		newocurrence.start_at = newocurrence.start_at + newocurrence.number_of_times.days
    	else
    		if newocurrence.every ==="Weeks"
    			newocurrence.start_at = newocurrence.start_at + newocurrence.number_of_times.weeks
    		else
    			if newocurrence.every ==="Months"
    				newocurrence.start_at = newocurrence.start_at + newocurrence.number_of_times.months
    			else 
            if newocurrence.every==="Years"
    				  newocurrence.start_at = newocurrence.start_at + newocurrence.number_of_times.years
            end
    			end
    		end
    	end
      newocurrence.save
    else
      self.complete = true
      self.save
    end
  end

  def next_ocurrence()
    if self.number_of_times>0
      if self.every === "Days"
        self.start_at + self.number_of_times.days
      else
        if self.every ==="Weeks"
          self.start_at + self.number_of_times.weeks
        else
          if self.every ==="Months"
            self.start_at + self.number_of_times.months
          else 
            if self.every==="Years"
             self.start_at + self.number_of_times.years
            end
          end
        end
      end
    end
  end

  before_save :default_values
  def default_values
    self.end_at = self.start_at + 1.hour
  end


end