# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
u1 = User.create(
  :email                => 'superadmin@rhodium.cl', 
  :password   => 'superadmin' ,
  :name   => 'superadmin' ,
  :password_confirmation        => 'superadmin',
  :location => 'Maipu',
  :role => "superadmin" 
) 
u2 = User.create(
  :email                => 'user@rhodium.cl', 
  :password   => 'user' ,
  :name   => 'user' ,
  :password_confirmation        => 'user',
  :location => 'Maipu',
  :role => "owner" 
)

# Confirm the user for Devise 
u1.confirm! 
u2.confirm!


Clinic.create( :name=>'Cedillo Clínica Veterinaria', :address=>'Av. Iv Centenario Nº 1047 ', :location=> 'Las Condes', :phone=>'2241712',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Colon Clínica Veterinaria', :address=>'Av. Colon Nº 5781', :location=> 'Las Condes', :phone=>'2117768 - 2119305',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Cuarto Centenario 807 Local 4', :address=>'', :location=> 'Las Condes', :phone=>'2029004',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Dog And Cat Clínica Veterinaria', :address=>'Gilberto Fuenzalida Nº229 Loca', :location=> 'Las Condes', :phone=>'2018095',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Integravet Clínica Veterinaria', :address=>'Latadia Nø 5017', :location=> 'Las Condes', :phone=>'2288100',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Oriente Clínica Veterinaria', :address=>'Av. Las Condes 7703', :location=> 'Las Condes', :phone=>'2024939',:person_in_charge=>'',:email=>'claudianunes22@gmail.com',:webpage=>'')
Clinic.create( :name=>'Pasteur Clínica Veterinaria', :address=>'Tomas Moro Nº 710', :location=> 'Las Condes', :phone=>'2249100',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet`S Center Clínica Veterinaria', :address=>'GILBERTO FUENZALIDA N º199 L16', :location=> 'Las Condes', :phone=>'2018631',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Petsalud Clínica Veterinaria', :address=>'Chesterton 8435', :location=> 'Las Condes', :phone=>'8134286',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'San Blas Clínica Veterinaria', :address=>'Av. Pdte. Errázuriz Nº 3078', :location=> 'Las Condes', :phone=>'3566813',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'San Damián Clínica Veterinaria', :address=>'Nueva Las Condes 12265 Lc-6', :location=> 'Las Condes', :phone=>'2151117',:person_in_charge=>'',:email=>'petscantagallo@gmail.com',:webpage=>'')
Clinic.create( :name=>'San Paolo Clínica Veterinaria', :address=>'Juan De Austria Nø 1851', :location=> 'Las Condes', :phone=>'2284710',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Tomas Moro Clínica Veterinaria', :address=>'Tomas Moro 1242', :location=> 'Las Condes', :phone=>'2011486 - 2295083',:person_in_charge=>'',:email=>'cvtomasmoro@gmail.com',:webpage=>'')
Clinic.create( :name=>'Total-Pet Clínica Veterinaria', :address=>'Av. Padre Hurtado Sur Nº 1807', :location=> 'Las Condes', :phone=>'9542683',:person_in_charge=>'',:email=>'www.total-pet.cl',:webpage=>'')
Clinic.create( :name=>'Vet Friend Clínica Veterinaria', :address=>'Patricia N°9106 Local-7', :location=> 'Las Condes', :phone=>'8847731',:person_in_charge=>'',:email=>'antivil@gmail.com',:webpage=>'')
Clinic.create( :name=>'Animal Pets', :address=>'Av. Kennedy 9001 Local 2101', :location=> 'Las Condes', :phone=>'2131225',:person_in_charge=>'',:email=>'jhasbun24@gmail.com',:webpage=>'')
Clinic.create( :name=>'Centro Veterinario Oriente', :address=>'Av. Alonso De Córdoba Nº 5491', :location=> 'Las Condes', :phone=>'2117566',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Centro Veterinario Padre Hurtado', :address=>'Av. Padre Hurtado Ce. Nº 695', :location=> 'Las Condes', :phone=>'2120935',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Clino Pet', :address=>'Av. Apoquindo 5182', :location=> 'Las Condes', :phone=>'2127172',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Comercial Carolina', :address=>'Rio Támesis 910', :location=> 'Las Condes', :phone=>'7104030',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Dog Phone', :address=>'Monroe Nº 7355', :location=> 'Las Condes', :phone=>'2469512',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Happy Dog', :address=>'Del Inca Nº 4679', :location=> 'Las Condes', :phone=>'3213187',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Home Vets', :address=>'Dra. Ernestina Pérez Nº 846', :location=> 'Las Condes', :phone=>'98379277',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Peluquería Andrea', :address=>'Apoquindo Nº6415 Local 2', :location=> 'Las Condes', :phone=>'2295414',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Perruqueria', :address=>'Padre Hurtado 1580', :location=> 'Las Condes', :phone=>'2205262',:person_in_charge=>'',:email=>'contacto@perruqueria.cl',:webpage=>'')
Clinic.create( :name=>'Pet Choice', :address=>'Manquehue Sur Nº31 Local 107', :location=> 'Las Condes', :phone=>'2010533',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet Club', :address=>'Av. Las Condes 9889 22-A', :location=> 'Las Condes', :phone=>'76025212',:person_in_charge=>'',:email=>'renec10@live.com',:webpage=>'')
Clinic.create( :name=>'Pet House Veterinaria', :address=>'Av. Fco. Bilbao N 8080 L-1 T-5', :location=> 'Las Condes', :phone=>'3254898',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet King', :address=>'Los Vilos Nø 1236', :location=> 'Las Condes', :phone=>'2114691',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet Life ', :address=>'San Crescente 416', :location=> 'Las Condes', :phone=>'89009576',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet Services Clínica Veterinaria ', :address=>'Luis Matte Larraín 483', :location=> 'Las Condes', :phone=>'89227326',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet Shop Doll', :address=>'Hernando De Magallanes 826', :location=> 'Las Condes', :phone=>'2245121',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet SHOP Don Bonini', :address=>'Longopilla 1579 - A', :location=> 'Las Condes', :phone=>'8810853',:person_in_charge=>'',:email=>'sebastian.trujillo.m@gmail.com',:webpage=>'')
Clinic.create( :name=>'Pet&Company', :address=>'Av. Kennedy 5413 Lc. 501-C', :location=> 'Las Condes', :phone=>'2244066',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'R & A Pet', :address=>'Apoquindo 7910 Local 3', :location=> 'Las Condes', :phone=>'2021974',:person_in_charge=>'',:email=>'ventas@rypet.cl',:webpage=>'')
Clinic.create( :name=>'Sandocan', :address=>'Av. Charles Hamilton 317', :location=> 'Las Condes', :phone=>'2112626',:person_in_charge=>'',:email=>'mantencion@sandocan.cl',:webpage=>'')
Clinic.create( :name=>'Sik-9 Soluciones Caninas', :address=>'Cerro La Parva 995 Dpto. 246', :location=> 'Las Condes', :phone=>'82398017',:person_in_charge=>'',:email=>'info@fik-9.com',:webpage=>'')
Clinic.create( :name=>'Sos Animal', :address=>'Presidente Riesco Nº 3058 L/1', :location=> 'Las Condes', :phone=>'3333135',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Vet Gatos Y Perros', :address=>'Latadia 4299', :location=> 'Las Condes', :phone=>'83406986',:person_in_charge=>'',:email=>'ducielm@gmail.com',:webpage=>'')
Clinic.create( :name=>'Vet Zoo', :address=>'Rio Ganges Nº 8793', :location=> 'Las Condes', :phone=>'7560099',:person_in_charge=>'',:email=>'dbunster@gmail.com',:webpage=>'')
Clinic.create( :name=>'Veterinaria Magallanes', :address=>'Av. Apoquindo 6988', :location=> 'Las Condes', :phone=>'7571706',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Veterinaria Manquehue', :address=>'Av. Isabel La Católica 6091', :location=> 'Las Condes', :phone=>'7580468',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Clínica Veterinaria Clinomovil', :address=>'Av. las condes # 8850 - A', :location=> 'Las Condes', :phone=>'2294718',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Clínica Veterinaria Dr. Pet', :address=>'Padre Hurtado 1621', :location=> 'Las Condes', :phone=>'2482272',:person_in_charge=>'',:email=>'www.doctorpet.cl',:webpage=>'')
Clinic.create( :name=>'Clínica Veterinaria My Friends', :address=>'Patricia #9106, local 7', :location=> 'Las Condes', :phone=>'4533975',:person_in_charge=>'Doctora Tatiana Jorquera',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet Country', :address=>'Bocaccio #511', :location=> 'Las Condes', :phone=>'7168599',:person_in_charge=>'',:email=>'ventas@pet-country.cl',:webpage=>'http://www.pet-country.cl/')
Clinic.create( :name=>'Pooch Boutique', :address=>'Domingo Bondi 1035', :location=> 'Las Condes', :phone=>'74303085',:person_in_charge=>'',:email=>'info@boutiquepooch.cl',:webpage=>'www.boutiquepooch.cl')
Clinic.create( :name=>'Centro Médico Veterinario Cedilo', :address=>'Av. Cuarto Centenario 1027 (Rotonda Atenas), ', :location=> 'Las Condes', :phone=>'2241712',:person_in_charge=>'',:email=>'cedilovet@gmail.com',:webpage=>'http://www.cedilo.cl')
Clinic.create( :name=>'Centro Veterinario Bayona', :address=>'Av. Tomás Moro 1905', :location=> 'Las Condes', :phone=>'2125103',:person_in_charge=>'',:email=>'pabanguita@hotmail.com',:webpage=>'')
Clinic.create( :name=>'Clínica Veterinaria Macdog', :address=>'Av. Cristóbal Colón 7940', :location=> 'Las Condes', :phone=>'2297412',:person_in_charge=>'',:email=>'import_export@macdog.cl',:webpage=>'http://www.macdog.cl')
Clinic.create( :name=>'Vet House Veterinaria', :address=>'Av. Francisco Bilbao 8080, local 1', :location=> 'Las Condes', :phone=>'3254898',:person_in_charge=>'',:email=>'vethouse@vethouse.cl',:webpage=>'http://www.vethouse.cl/')


Clinic.create( :name=>'Animal Clinic Clínica Veterinaria', :address=>'Av.Ossa 582', :location=> ' Ñuñoa', :phone=>'4927952',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Hamburgo Clínica Veterinaria', :address=>'Hamburgo 432', :location=> ' Ñuñoa', :phone=>'7270316',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Macul Clínica Veterinaria', :address=>'Los Alerces 3135', :location=> ' Ñuñoa', :phone=>'2386440',:person_in_charge=>'',:email=>'veterinariamacul@yahoo.com',:webpage=>'')
Clinic.create( :name=>'Marceven Clínica Veterinaria', :address=>'Av. Grecia Nº 1730', :location=> ' Ñuñoa', :phone=>'2389948',:person_in_charge=>'',:email=>'Clínica@marceven.cl',:webpage=>'')
Clinic.create( :name=>'Metropolitana Clínica Veterinaria', :address=>'Av. Irarrázaval 713', :location=> ' Ñuñoa', :phone=>'2235702',:person_in_charge=>'',:email=>'rgrudsky@gmail.com',:webpage=>'')
Clinic.create( :name=>'My Vet Clínica Veterinaria', :address=>'Av. Suecia 2860', :location=> ' Ñuñoa', :phone=>'7520437',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Nueva Madrid Clínica Veterinaria', :address=>'Simón Bolívar 2645', :location=> ' Ñuñoa', :phone=>'2748865',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pravet Clínica Veterinaria', :address=>'Itata Nº 4250', :location=> ' Ñuñoa', :phone=>'91387542-4589616',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Tobalaba Clínica Veterinaria', :address=>'Avenida Ossa Nº 1564', :location=> ' Ñuñoa', :phone=>'2276015',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'(+) Cotas', :address=>'Sucre 2712 Local 2', :location=> ' Ñuñoa', :phone=>'7163685',:person_in_charge=>'',:email=>'contacto@mas-cotas.cl',:webpage=>'')
Clinic.create( :name=>'Alemán Hospital Veterinario ', :address=>'Simón Bolívar Nº 2197', :location=> ' Ñuñoa', :phone=>'7699409',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Cachorros Express', :address=>'Av. Irarrázaval N°1815', :location=> ' Ñuñoa', :phone=>'3413460',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Canicat', :address=>'Av. Simón Bolívar Nº 2919', :location=> ' Ñuñoa', :phone=>'3262280',:person_in_charge=>'',:email=>'canicat@canicat.com',:webpage=>'')
Clinic.create( :name=>'Cidem', :address=>'Dr. Hernán Cortes 3340', :location=> ' Ñuñoa', :phone=>'4580952',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Clínica Dr. Zoo', :address=>'Pedro Torres 693', :location=> ' Ñuñoa', :phone=>'2239563',:person_in_charge=>'',:email=>'vriveroszoo@gmail.com',:webpage=>'')
Clinic.create( :name=>'Clínica Veterinaria', :address=>'Unión Literaria Nº1874', :location=> ' Ñuñoa', :phone=>'3561759',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Distribuidora Los Robles', :address=>'Pedro De Valdivia 3398', :location=> ' Ñuñoa', :phone=>'6343603',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Jardín Zoo', :address=>'Irarrázaval 3799', :location=> ' Ñuñoa', :phone=>'2232496',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Lavacan Express', :address=>'Simón Bolívar 4510', :location=> ' Ñuñoa', :phone=>'8938898',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Mascotilandy', :address=>'Pedro De Valdivia 5461', :location=> ' Ñuñoa', :phone=>'8677116',:person_in_charge=>'',:email=>'mascotilandy@gmail.com',:webpage=>'')
Clinic.create( :name=>'Monttarnasse', :address=>'Manuel Montt 2283', :location=> ' Ñuñoa', :phone=>'2046196',:person_in_charge=>'',:email=>'contacto@perruqueria.cl',:webpage=>'')
Clinic.create( :name=>'Ñuñoa Clínica Veterinaria ', :address=>'Av. Irarrázaval 4122', :location=> ' Ñuñoa', :phone=>'2049187',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Patitas Vet', :address=>'Av. Grecia Nº 3626', :location=> ' Ñuñoa', :phone=>'2394242',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pedro De Valdivia Hospital Veter…', :address=>'Eduardo Castillo V. 3061', :location=> ' Ñuñoa', :phone=>'3433653',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Perrufus Peluquería Y Alimento P...', :address=>'Echeñique 5010', :location=> ' Ñuñoa', :phone=>'2738301',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pet And Pet', :address=>'Francisco Villagra 122', :location=> ' Ñuñoa', :phone=>'8856481',:person_in_charge=>'',:email=>'pets8pets@gmail.com',:webpage=>'')
Clinic.create( :name=>'Petcan ', :address=>'Campo De Deporte N°114', :location=> ' Ñuñoa', :phone=>'8849169',:person_in_charge=>'',:email=>'contacto@petcan.cl',:webpage=>'')
Clinic.create( :name=>'Punto Mascotas', :address=>'Suarez Mujica 1388', :location=> ' Ñuñoa', :phone=>'8488558',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Venta De Alimentos', :address=>'Rengo 736', :location=> ' Ñuñoa', :phone=>'2254909',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Veterinaria Beautypet', :address=>'Exequiel Fernández 1729', :location=> ' Ñuñoa', :phone=>'8856475',:person_in_charge=>'',:email=>'veterinariabeautypet@veterinaria.cl',:webpage=>'')
Clinic.create( :name=>'Veterinaria Dora Muller', :address=>'Los Avellanos, 2436', :location=> ' Ñuñoa', :phone=>'2383866',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Vetline Clínica Veterinaria', :address=>'Julio Zegers 3977', :location=> ' Ñuñoa', :phone=>'2696416',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Vicky Pet', :address=>'Coventry Nº  716', :location=> ' Ñuñoa', :phone=>'2269268',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Bodega Zañartu', :address=>'Avenida Zañartu 2558', :location=> ' Ñuñoa', :phone=>'3569739',:person_in_charge=>'',:email=>'bodegazanartu@vtr.net',:webpage=>'')
Clinic.create( :name=>'DISTRIBUIDORA CAROCA`S PET', :address=>'La Fortuna # 520 Local 3', :location=> ' Ñuñoa', :phone=>'9964381',:person_in_charge=>'',:email=>'c.caroca@live.cl',:webpage=>'www.carocaspet.cl')


Clinic.create( :name=>'Buen Pastor Clínica Veterinaria', :address=>'Av. La Farfana Nº 1338', :location=> 'Maipú', :phone=>'7625686 - 95172884',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'El Vergel Clínica Veterinaria', :address=>'Capellán Florencio Infante 2770', :location=> 'Maipú', :phone=>'8863870',:person_in_charge=>'',:email=>'veterinariaelvergel@gmail.com',:webpage=>'')
Clinic.create( :name=>'Hundeland Clínica Veterinaria', :address=>'Av. Parque Central Oriente 1303', :location=> 'Maipú', :phone=>'5373849',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Idan Clínica Veterinaria', :address=>'Capricornio 855', :location=> 'Maipú', :phone=>'5320665',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'A Y C', :address=>'Av. Pajaritos 1499 - A', :location=> 'Maipú', :phone=>'4174512',:person_in_charge=>'',:email=>'alimentos.ayc@gmail.com',:webpage=>'')
Clinic.create( :name=>'Cav Maipú', :address=>'Av. Pajaritos 404', :location=> 'Maipú', :phone=>'3230025',:person_in_charge=>'',:email=>'cavmaipu@gmail.com',:webpage=>'')
Clinic.create( :name=>'Comercializadora Animal Food', :address=>'Blanco Encalada 1002', :location=> 'Maipú', :phone=>'7666116',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Dr. Norton', :address=>'Carmen Nº1050', :location=> 'Maipú', :phone=>'5316734 - 98409066',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'El Rincón De La Mascota', :address=>'Av. Nueva San Martin 1330 - C', :location=> 'Maipú', :phone=>'5325382',:person_in_charge=>'',:email=>'consultas@elrincondelamascota.cl',:webpage=>'')
Clinic.create( :name=>'Nirza Odelia Silva', :address=>'Av. Parque Central Oriente 806', :location=> 'Maipú', :phone=>'5372296',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Pop Animal', :address=>'Etiopia 827 Villa El Abrazo', :location=> 'Maipú', :phone=>'8950483',:person_in_charge=>'',:email=>'pam-vet@hotmail.com',:webpage=>'')
Clinic.create( :name=>'San Francisco De Asís', :address=>'Lumen 3644', :location=> 'Maipú', :phone=>'8851976',:person_in_charge=>'',:email=>'lalita_sil@hotmail.com',:webpage=>'')
Clinic.create( :name=>'Santa Gema Clínica Veterinaria ', :address=>'Av. Pajaritos Nº 4266', :location=> 'Maipú', :phone=>'4198874',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Sasari', :address=>'Pasaje Las Garzas 584', :location=> 'Maipú', :phone=>'4173395',:person_in_charge=>'',:email=>'perroatropellao@hotmail.com',:webpage=>'')
Clinic.create( :name=>'Sofauna', :address=>'Raúl Mazzone 3031', :location=> 'Maipú', :phone=>'7667414',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Torrealba Hermanos', :address=>'Monumento 1790', :location=> 'Maipú', :phone=>'5481381',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'Veterinaria Cat Y Can', :address=>'9273907 Maipú', :location=> 'Maipú', :phone=>'3169351',:person_in_charge=>'',:email=>'vetcatycan@gmail.com',:webpage=>'')
Clinic.create( :name=>'Veterinaria Catdog', :address=>'La Opera 356', :location=> 'Maipú', :phone=>'7445348',:person_in_charge=>'',:email=>'veterinariacatdog@gmail.com',:webpage=>'')
Clinic.create( :name=>'Veterinaria Dr. Leonel Cáceres', :address=>'Av. Pajaritos 3456', :location=> 'Maipú', :phone=>'5341996',:person_in_charge=>'',:email=>'info@clinica-veterinaria.cl',:webpage=>'')
Clinic.create( :name=>'Veterinaria Maipú', :address=>'Av. Pajaritos 3456', :location=> 'Maipú', :phone=>'5341996',:person_in_charge=>'',:email=>'maipu.veterinaria@gmail.com',:webpage=>'')
Clinic.create( :name=>'Veterinaria Peluquería Canina Yasis', :address=>'Av. Las Industrias 16411 ', :location=> 'Maipú', :phone=>'5358094',:person_in_charge=>'',:email=>'macaruvet@gmail.com',:webpage=>'')
Clinic.create( :name=>'Zoo Market', :address=>'Pajaritos 2130 Local-5', :location=> 'Maipú', :phone=>'7667146',:person_in_charge=>'',:email=>'',:webpage=>'')
Clinic.create( :name=>'HAPPY DOG', :address=>'AVENIDA EL ALTO 1302', :location=> 'Maipú', :phone=>'77458948',:person_in_charge=>'',:email=>'N.MARDONES.23@HOTMAIL.COM',:webpage=>'')
Clinic.create( :name=>'K-CAN', :address=>'Av. Portales n°205, local 4, MAIPU', :location=> 'Maipú', :phone=>'9868304',:person_in_charge=>'',:email=>'cereceda.german@gmail.com',:webpage=>'')
Clinic.create( :name=>'Peluquería Kidamy', :address=>'Pje. Capricornio 855', :location=> 'Maipú', :phone=>'5320665',:person_in_charge=>'',:email=>'',:webpage=>' www.kidamy.cl')
Clinic.create( :name=>'world pet´s', :address=>'esquina blanca 093', :location=> 'Maipú', :phone=>'78850813',:person_in_charge=>'',:email=>'anitaspicder@gmail.com',:webpage=>'')

