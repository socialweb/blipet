# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121018014955) do

  create_table "calendar_events", :force => true do |t|
    t.string   "name"
    t.datetime "start_at"
    t.datetime "end_at"
    t.boolean  "all_day",         :default => false
    t.datetime "last_ocurrence"
    t.integer  "number_of_times"
    t.string   "every"
    t.integer  "pet_id"
    t.boolean  "complete",        :default => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "calendar_events", ["pet_id"], :name => "index_calendar_events_on_pet_id"

  create_table "clinics", :force => true do |t|
    t.string   "name"
    t.string   "location"
    t.string   "address"
    t.string   "webpage"
    t.integer  "phone"
    t.string   "email"
    t.string   "person_in_charge"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "gmaps"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "comments", :force => true do |t|
    t.integer  "event_id"
    t.integer  "owner_id"
    t.text     "text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "deparasitation_pets", :force => true do |t|
    t.integer  "pet_id"
    t.boolean  "dep_type"
    t.string   "dep_brand"
    t.date     "dep_date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "deparasitation_pets", ["pet_id"], :name => "index_deparasitation_pets_on_pet_id"

  create_table "disease_pets", :force => true do |t|
    t.string   "name"
    t.integer  "pet_id"
    t.string   "medication"
    t.boolean  "cronic"
    t.date     "diagnostic_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "disease_pets", ["pet_id"], :name => "index_disease_pets_on_pet_id"

  create_table "events", :force => true do |t|
    t.integer  "pet_id"
    t.string   "event_type"
    t.date     "date"
    t.text     "event_text"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "location"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "pets", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.boolean  "gender"
    t.string   "specie"
    t.string   "race"
    t.date     "b_date"
    t.integer  "privacy_level",       :default => 3
    t.float    "weight_last"
    t.float    "weight_actual"
    t.string   "food_type"
    t.integer  "food_dosis"
    t.string   "food_extras"
    t.string   "food_brand"
    t.integer  "id_rabies"
    t.integer  "id_pedigree"
    t.integer  "id_microchip"
    t.string   "exercise_type"
    t.string   "exercise_location"
    t.string   "hair_dreeser"
    t.string   "hair_type"
    t.string   "hair_cut_type"
    t.string   "bath_hairdresser"
    t.string   "san_bath"
    t.string   "san_bath_prod"
    t.boolean  "sterilyzed"
    t.integer  "clinic_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "pets", ["clinic_id"], :name => "index_pets_on_clinic_id"
  add_index "pets", ["user_id"], :name => "index_pets_on_user_id"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 5
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "sales", :force => true do |t|
    t.string   "brand"
    t.date     "end_at"
    t.string   "promo_text"
    t.string   "promo_image_file_name"
    t.string   "promo_image_content_type"
    t.integer  "promo_image_file_size"
    t.datetime "promo_image_updated_at"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "role"
    t.string   "location"
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.boolean  "gender"
    t.string   "provider"
    t.string   "uid"
    t.string   "username"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "privacy_level",          :default => 3
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "vaccine_pets", :force => true do |t|
    t.integer  "pet_id"
    t.string   "name"
    t.integer  "vaccine_id"
    t.date     "vaccine_date"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "vaccine_pets", ["pet_id"], :name => "index_vaccine_pets_on_pet_id"

end
