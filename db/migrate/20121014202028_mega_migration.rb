class MegaMigration < ActiveRecord::Migration
  def change
    
	#USERS
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      ## Token authenticatable
      t.string :authentication_token

      ## Extra Info
      t.string :role
      t.string :location
      t.string :name
      t.string :address
      t.string :phone
      t.boolean :gender
      t.string :provider
      t.string :uid
      t.string :username
      t.float :latitude
      t.float :longitude
      t.integer :privacy_level, :default => 3

      t.has_attached_file :avatar

      t.timestamps
    end

    add_index :users, :email,                :unique => true
    add_index :users, :reset_password_token, :unique => true
    add_index :users, :confirmation_token,   :unique => true
    add_index :users, :unlock_token,         :unique => true
    add_index :users, :authentication_token, :unique => true
  

    #PETS
    create_table :pets do |t|
      t.references :user
      t.string :name
      t.boolean :gender
      t.string :specie
      t.string :race
      t.date :b_date
      t.integer :privacy_level, :default => 3

      #info
      t.float :weight_last
      t.float :weight_actual

      t.string :food_type
      t.integer :food_dosis
      t.string :food_extras
      t.string :food_brand


      t.integer :id_rabies
      t.integer :id_pedigree
      t.integer :id_microchip

      t.string :exercise_type
      t.string :exercise_location

      t.string :hair_dreeser
      t.string :hair_type
      t.string :hair_cut_type

      t.string :bath_hairdresser
      t.string :san_bath
      t.string :san_bath_prod

      t.boolean :sterilyzed

      t.references :clinic
      t.integer :clinic_id
      t.has_attached_file :avatar

      t.timestamps
    end
    add_index :pets, :user_id
    add_index :pets, :clinic_id

    #DEPARASITATION
    create_table :deparasitation_pets do |t|
      t.references :pet
      t.integer :pet_id
      t.boolean :dep_type
      t.string :dep_brand
      t.date :dep_date

      t.timestamps
    end
    add_index :deparasitation_pets, :pet_id


    #VACCINES
    create_table :vaccine_pets do |t|
      t.references :pet
      t.integer :pet_id
      t.string :name
      t.integer :vaccine_id
      t.date :vaccine_date

      t.timestamps
    end
    add_index :vaccine_pets, :pet_id


    #DISEASES
    create_table :disease_pets do |t|
      t.string :name
      t.references :pet
      t.integer :pet_id
      t.string :medication
      t.boolean :cronic
      t.date :diagnostic_date

      t.timestamps
    end
    add_index :disease_pets, :pet_id


    #EVENTS
    create_table :events do |t|
      t.integer :pet_id
      t.string :event_type
      t.date :date
      t.text :event_text
      t.has_attached_file :photo
      t.string :location
      t.float :latitude
      t.float :longitude

      t.timestamps
    end

    create_table :comments do |t|
      t.integer :event_id
      t.integer :owner_id
      t.text :text
      t.timestamps
    end

    #RAILS ADMIN
    create_table :rails_admin_histories do |t|
       t.text :message # title, name, or object_id
       t.string :username
       t.integer :item
       t.string :table
       t.integer :month, :limit => 2
       t.integer :year, :limit => 5
       t.timestamps
    end
    add_index(:rails_admin_histories, [:item, :table, :month, :year], :name => 'index_rails_admin_histories' )


	#CLINICS
    create_table :clinics do |t|
      t.string :name
      t.string :location
      t.string :address
      t.string :webpage
      t.integer :phone
      t.string :email
      t.string :person_in_charge
      t.has_attached_file :avatar
      t.has_attached_file :photo
      t.float :latitude
      t.float :longitude
      t.boolean :gmaps

      t.timestamps
    end

    create_table :calendar_events do |t|
      t.string :name
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :all_day, :default => false
      t.datetime :last_ocurrence
      t.integer :number_of_times
      t.string :every
      t.references :pet
      t.integer :pet_id
      t.boolean :complete, :default => false
      
      t.timestamps
    end
    add_index :calendar_events, :pet_id

  end
end
