class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :brand
      t.date :end_at
      t.string :promo_text
      t.has_attached_file :promo_image

      t.timestamps
    end
  end
end
